<?php
/**
 * Administrtative functions for multistep blocks
 */
module_load_include('inc', 'block', 'block.admin');

/**
 * Add a multistep block
 */
function multistep_block_add_block_form(&$form_state) {
  return block_admin_configure($form_state, 'multistep_block', NULL);
}

/**
 * Save the multistep block to the database
 */
function multistep_block_add_block_form_submit($form, &$form_state) {
  $multistep_block = array(
    'steps' => check_plain($form_state['values']['steps']),
    'pass_arguments' => check_plain($form_state['values']['arguments']),
  );
  drupal_write_record('multistep_block', $multistep_block);

  $mbid = db_last_insert_id('multistep_block', 'mbid');
  $delta = "multistep_block_$mbid";

  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, '%s', %d)", $form_state['values']['visibility'], trim($form_state['values']['pages']), $form_state['values']['custom'], $form_state['values']['title'], $form_state['values']['module'], $theme->name, 0, 0, $delta, BLOCK_NO_CACHE);
    }
  }

  foreach (array_filter($form_state['values']['roles']) as $rid) {
    db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')", $rid, $form_state['values']['module'], $delta);
  }

  drupal_set_message(t('The block has been created.'));
  cache_clear_all();

  $form_state['redirect'] = 'admin/build/block';
  return;
}

/**
 * Delete the multistep block
 */
function multistep_block_delete(&$form_state, $delta = 0) {
 
  $matches = array();
  preg_match('/_[0-9]+$/', $delta, $matches);
  $mbid = substr($matches[0], 1);
 
  $form['info'] = array(
    '#type' => 'hidden',
    '#value' => "Multistep block $mbid",
  );
  $form['mbid'] = array(
    '#type' => 'hidden',
    '#value' => $mbid,
  );

  return confirm_form($form, t('Are you sure you want to delete the block %name?', array('%name' => $form['info']['#value'])), 'admin/build/block', '', t('Delete'), t('Cancel'));
}

/**
 * Really delete the thing
 */
function multistep_block_delete_submit($form, &$form_state) {
  db_query("DELETE FROM {blocks} WHERE module = 'multistep_block' AND delta = '%s'", "multistep_block_{$form_state['values']['mbid']}");
  db_query("DELETE FROM {multistep_block} WHERE mbid = %d", $form_state['values']['mbid']);
  drupal_set_message(t('The block %name has been removed.', array('%name' => $form_state['values']['info'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/build/block';
  return;
}
